# Generated files

All files from within `output/` folder built from other sources. Do not
attempt to modify them by hand.

You should include the generated files with your CR but never to
edit them manually.
