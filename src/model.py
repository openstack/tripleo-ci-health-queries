"""Models used for validation."""
from pathlib import Path
from typing import List, Optional, Union

from pydantic import BaseModel, Extra, Field, HttpUrl


class Query(BaseModel):
    """Query Model."""

    id: str
    name: Optional[str]
    pattern: Optional[Union[List[str], str]]

    category: Optional[str]
    url: Optional[Union[List[HttpUrl], HttpUrl]]

    # elastic-search specific fields
    logstash: Optional[str]
    tags: Optional[Union[List[str], str]]
    build_name: Optional[Union[List[str], str]]
    not_build_name: Optional[Union[List[str], str]]
    build_status: Optional[Union[List[str], str]]
    filename: Optional[Union[List[str], str]]
    voting: Optional[str]
    suppress_notification: Optional[bool] = Field(
        alias="suppress-notification", description="Used for gerrit bot"
    )
    suppress_graph: Optional[bool] = Field(
        alias="suppress-graph", description="Used for elastic-recheck"
    )

    # artcl/sove specific fields
    regex: Optional[Union[List[str], str]]
    msg: Optional[Union[List[str], str]]
    sova_tag: Optional[str]
    error_type: Optional[str]
    # https://opendev.org/openstack/ansible-role-collect-logs/src/branch/master/vars/sova-patterns.yml#L47
    files: Optional[List[str]] = Field(
        description="List of glob patterns, narrows down searching"
    )

    class Config:
        """Disalow unknown attributes."""

        extra = Extra.forbid


class Queries(BaseModel):
    """Queries Model."""

    queries: List[Query]


# this is equivalent to json.dumps(MainModel.schema(), indent=2):
my_dir = Path(__file__).resolve().parents[1]
output_file = my_dir / "output" / "queries-schema.json"
with open(output_file, "w") as f:
    f.write(Queries.schema_json(indent=2))
    f.write("\n")
