"""Generates Sova compatible json file from human readable queries.

It takes 'regex' or if 'regex' is not available it converts 'pattern' to regex,
from input file and forms sova compatible json file.

input: src/data/queries.yml
output: output/sova-pattern-generated.json
"""
import json
import os
import re

import yaml

dir_path = os.path.dirname(os.path.realpath(__file__))
# Source and destination files
queries_src = os.path.join(dir_path, "data", "queries.yml")
sova_dest = os.path.join(
    os.path.dirname(dir_path), "output", "sova-pattern-generated.json"
)

with open(queries_src) as in_file:
    queries_list = yaml.load(in_file, Loader=yaml.BaseLoader)

sova_regex_list = []
sova_patterns_list = {}
sova_dict = {"regexes": sova_regex_list, "patterns": sova_patterns_list}

for query_dict in queries_list["queries"]:
    regex_dict = {}
    regex_str = query_dict.get("regex", "")
    regex_name = "_".join(query_dict["id"].split(" "))
    regex_dict_list = []
    if {"regex", "pattern"} <= query_dict.keys():
        # No pattern/regex conversion
        regex_dict = {"name": regex_name, "regex": query_dict["regex"]}
        regex_dict_list.append(regex_dict)
    elif "regex" in query_dict:
        regex_dict = {"name": regex_name, "regex": query_dict["regex"]}
        regex_dict_list.append(regex_dict)
    elif "pattern" in query_dict:
        # Convert pattern to regex for Sova
        if isinstance(query_dict["pattern"], str):
            generated_regex = re.escape(query_dict["pattern"])
            regex_dict = {"name": regex_name, "regex": generated_regex}
            regex_str = generated_regex
            regex_dict_list.append(regex_dict)
        elif isinstance(query_dict["pattern"], list):
            generated_regex = []
            index = 0
            for item in query_dict["pattern"]:
                generated_regex = re.escape(item)
                regex_dict = {
                    "name": regex_name + "_" + str(index),
                    "regex": generated_regex,
                }
                regex_dict_list.append(regex_dict)
                index += 1
    else:
        continue
    sova_regex_list.extend(regex_dict_list)
    sova_tag = query_dict.get("sova_tag", "console")
    patterns = sova_patterns_list.get(sova_tag, list())
    msg = query_dict.get("msg", regex_name)
    error_type = query_dict.get("error_type", "info")
    pattern_dict_list = []
    if len(regex_dict_list) > 1:
        for item in regex_dict_list:
            pattern_dict = {
                "id": item["name"],
                "logstash": "",
                "msg": msg,
                "pattern": item["name"],
                "tag": error_type,
            }
            pattern_dict_list.append(pattern_dict)
    else:
        pattern_dict = {
            "id": regex_name,
            "logstash": "",
            "msg": msg,
            "pattern": regex_name,
            "tag": error_type,
        }
        pattern_dict_list.append(pattern_dict)
    # if regex_str:
    patterns.extend(pattern_dict_list)
    sova_patterns_list[sova_tag] = patterns

with open(sova_dest, "w") as out_file:
    json.dump(sova_dict, out_file, indent=4, sort_keys=True)
    # Adds newline to pass lint
    out_file.write("\n")
